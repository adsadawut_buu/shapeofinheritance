/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapesofinheritance;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public  Triangle(double base,double height){
        super();
        this.base = base;
        this.height = height;
        System.out.println("Triangle Created");
    }
    
    @Override
    public double calArea(){
        return (base*height)/2;
    }
    
    @Override
    public void print(){
        System.out.println("Calarea of Triangle: "+ calArea());
    }
}
