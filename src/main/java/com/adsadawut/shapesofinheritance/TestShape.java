/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapesofinheritance;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.calArea();
        circle1.print();
        
        Circle circle2 = new Circle(4);
        circle2.calArea();
        circle2.print();
        
        Triangle triangle1 = new Triangle(4,3);
        triangle1.print();
        
        Rectangle rectangle1 = new Rectangle(3,4);
        rectangle1.print();
        
        Square square1 = new Square(5);
        square1.print();
        
        System.out.println("square1 is Rectangle: "+ (square1 instanceof Rectangle));
        System.out.println("square1 is Shape: "+(square1 instanceof Shape));
        
        Shape shape1[]={circle1,circle2,triangle1,rectangle1,square1};
        for(int i =0;i<shape1.length;i++){
            shape1[i].print();
        }
        System.out.println("Finish Project!!!");
    }
    
}
