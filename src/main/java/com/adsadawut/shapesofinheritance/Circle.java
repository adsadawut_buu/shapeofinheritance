/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapesofinheritance;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Circle extends Shape {
    private double r;
    public static final double pi = 22.0/7;
    
    public Circle(double r){
        super();
        this.r=r;
        System.out.println("Circle Created");
    }
    
    @Override
    public double calArea(){
        return pi*r*r;
    }
    
    @Override
    public void print(){
        super.print();
        System.out.println("Calarea of Circle: "+calArea());
    }
    
}
