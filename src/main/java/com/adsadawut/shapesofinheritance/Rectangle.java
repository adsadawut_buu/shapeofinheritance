/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapesofinheritance;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Rectangle extends Shape{
    protected double width;
    protected double length;
    
    public Rectangle(double width,double length){
        super();
        this.width=width;
        this.length=length;
        System.out.println("Rectangle Created");
    }
    
    @Override
    public double calArea(){
        return width*length;
    }
    
    @Override
    public void print(){
        System.out.println("Calarea of Rectangle: "+ calArea());
    }
}
